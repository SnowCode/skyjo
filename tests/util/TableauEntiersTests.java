package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

class TableauEntiersTests {

	@Test
	void testValeursAleatoires() {
		Aleatoire.specifierNombres(7, 10, -2, 0);
		int[] tableauAttendu = {7,10,-2,0};
		assertArrayEquals(tableauAttendu, TableauEntiers.valeursAleatoires(4, -2, 12));
		
		// Lorsque nbValeurs vaut 0 alors le tableau retourné est vide
		int[] tableauVide = {};
		assertArrayEquals(tableauVide, TableauEntiers.valeursAleatoires(0, -2, 12));
	}
	
	@Test
	void testValeursAleatoiresPlusieursLignes() {
		Aleatoire.specifierNombres(7,10,-2,0,7,0,3,12);
		int[][] tableauAttendu = {{7,10,-2,0},{7,0,3,12}};
		assertArrayEquals(tableauAttendu, TableauEntiers.valeursAleatoires(2, 4, -2, 12));
		
		// si nbLignes et nbColonnes sont à 0 alors le tableau retourné est vide
		int[][] tableauVide = {};
		assertArrayEquals(tableauVide, TableauEntiers.valeursAleatoires(0, 0, -2, 12));
	}
	
	@Test
	void testSommer() {
		int[][] tableauDeBase = {{2, 8, 1}, {4, -2}};
		assertEquals(13, TableauEntiers.sommer(tableauDeBase));
		
		// Si le tableau est vide alors la somme est 0
		int[][] tableauVide = {};
		assertEquals(0, TableauEntiers.sommer(tableauVide));
		
		// Si le tableau est un tableau de lignes vide alors la somme est 0
		int[][] tableauLigneVide = {{},{}};
		assertEquals(0, TableauEntiers.sommer(tableauLigneVide));
	}
	
	@AfterEach
	void reinitialiserNombresAleatoires() {
	 Aleatoire.effacerNombres();
	}


}
