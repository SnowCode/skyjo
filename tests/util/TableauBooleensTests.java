package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TableauBooleensTests {

	@Test
	void testCompter() {
		boolean[][] tableauInput = {{false,true,true},{true,false,false}};
		assertEquals(3, TableauBooleens.compter(tableauInput, true));
		
		// Si c'est un tableau vide
		boolean[][] tableauVide = {};
		assertEquals(0, TableauBooleens.compter(tableauVide, true));
		
		// Si c'est un tableau de ligne vides
		boolean[][] tableauLigneVide = {{},{}};
		assertEquals(0, TableauBooleens.compter(tableauLigneVide, true));
	}

}
