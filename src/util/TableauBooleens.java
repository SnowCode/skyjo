package util;

public class TableauBooleens {
	/**
	 * Compte le nombre d'occurences d'une certaine valeur booléenne d'un tableau à deux dimensions
	 * @param t Le tableau à deux dimensions
	 * @param valeur La valeur à rechercher
	 * @return Le nombre d'occurence de la valeur dans le tableau
	 */
	public static int compter(boolean[][] t, boolean valeur) {
		int compteur = 0;
		for (boolean[] tableau: t) {
			for (boolean element: tableau) {
				if (element == valeur) {
					compteur++;
				}
			}
		}
		return compteur;
	}
}
