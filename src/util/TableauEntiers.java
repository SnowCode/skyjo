package util;

public class TableauEntiers {
	/**
	 * Génère un tableau contenant un certain nombre de valeurs aléatoires qui sont entre un minimum et un maximum défini
	 * @param nbValeurs Nombre de valeurs du tableau
	 * @param min Valeur minimum
	 * @param max Valeur maximum
	 * @return Le tableau d'entiers remplis de valeurs aléatoires
	 */
	public static int[] valeursAleatoires(int nbValeurs, int min, int max) {
		int[] tableauAleatoire = new int[nbValeurs];
		for (int i = 0; i < tableauAleatoire.length; i++) {
			tableauAleatoire[i] = Aleatoire.aleatoire(min, max);
		}
		return tableauAleatoire;
	}
	
	/**
	 * Génère un tableau à deux dimensions remplis de valeurs d'entiers alétatoires
	 * @param nbLignes Nombre de lignes du tableau
	 * @param nbColonnes Nombre de colonnes du tableau
	 * @param min Valeur minimum
	 * @param max Valeur maximum
	 * @return Le tableau à deux dimensions remplis de valeurs aléatoires
	 */
	public static int[][] valeursAleatoires(int nbLignes, int nbColonnes, int min, int max) {
		int[][] tableauAleatoire = new int[nbLignes][nbColonnes];
		for (int i = 0; i < tableauAleatoire.length; i++) {
			tableauAleatoire[i] = valeursAleatoires(nbColonnes, min, max);
		}
		return tableauAleatoire;
	}
	
	/**
	 * Effectue la somme de tous les sous-élément sd'un tableau à deux dimensions
	 * @param t Le tableau à deux dimension
	 * @return Le résultat de la somme
	 */
	public static int sommer(int[][] t) {
		int somme = 0;
		for (int[] tableau: t) {
			for (int element: tableau) {
				somme += element;
			}
		}
		return somme;
	}
}
