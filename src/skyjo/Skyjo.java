package skyjo;

import java.util.Arrays;

import io.Console;
import util.Aleatoire;
import util.TableauBooleens;
import util.TableauEntiers;

public class Skyjo {
	public static void main(String[] args) {
		// Créer la grille
		int[][] grille = TableauEntiers.valeursAleatoires(3, 4, -2, 12);
		
		// Faire en sorte que tout soit caché par défault
		boolean[][] visibilite = new boolean[3][4];
		for (int i = 0; i < visibilite.length; i++) {
			Arrays.fill(visibilite[i], false);
		}
		
		// Demander pour révèler les deux premières cartes
		afficherGrille(grille, visibilite);
		for (int i = 0; i < 2; i++) {
			int[] position = lirePosition("\nPosition de la carte à révéler ? ", visibilite);
			visibilite[position[0]][position[1]] = true;
			afficherGrille(grille, visibilite);
		}
		
		// Faire chaque manche
		int compteurCartes = 1;
		int carte, choix;
		int[] position;
		while (TableauBooleens.compter(visibilite, true) < 12 && compteurCartes <= 20) {
			// Indiquer le compteur de cartes
			System.out.printf("%nCarte n°%d/20%n", compteurCartes);
			// Tirage d'une carte
			carte = Aleatoire.aleatoire(-2, 12);
			System.out.printf("Vous tirez une carte de valeur %d%n", carte);
			compteurCartes++;
			
			
			// Demander à l'utilisateur que faire
			choix = lireChoix("(E)changer, (R)évéler ? ", "ER");
			if (choix == 'E') {
				position = lirePosition("Position de la carte à remplacer ? ");
				System.out.printf("Une carte de valeur %d est retirée de la grille.%n", grille[position[0]][position[1]]);
				grille[position[0]][position[1]] = carte;
				visibilite[position[0]][position[1]] = true;
			} else {
				position = lirePosition("Position de la carte à révéler ? ", visibilite);
				visibilite[position[0]][position[1]] = true;
			}
			
			// Afficher la grille
			afficherGrille(grille, visibilite);
		}
		
		// Indiquer le score
		int score = TableauEntiers.sommer(grille);
		System.out.printf("Votre score est de %d point(s).", score);
		
	}
	
	/**
	 * Affiche la grille en texte dans le terminal en cachant les cases qui doivent être cachées
	 * @param grille Grille à afficher
	 * @param visibiliteCartes Le tableau définissant si chaque position doit être visible ou non
	 */
	public static void afficherGrille(int[][] grille, boolean[][] visibiliteCartes) {
		String separateurLignes = "  +" + "----+".repeat(grille[0].length);
		System.out.print("     ");
		for (int i = 0; i < grille[0].length; i++) {
			System.out.printf("%d    ", i + 1);
		}
		System.out.println("");
		for (int i = 0; i < grille.length; i++) {
			System.out.println(separateurLignes);
			System.out.printf("%c ", i + 'A');
			for (int j = 0; j < grille[i].length; j++) {
				if (visibiliteCartes[i][j]) {
					System.out.printf("| %2d ", grille[i][j]);
				} else {
					System.out.print("| ## ");
				}
			}
			System.out.println("|");
		}
		System.out.println(separateurLignes);
	}
	
	/**
	 * Pose une question et demande la position dans la grille, ensuite transforme la position en tableau d'entiers correspodant aux indices de la grille
	 * @param question String posant la question à l'utilisateur
	 * @return Le tableau d'entier correspondant à la position dans la grille
	 */
	public static int[] lirePosition(String question) {
		String positionUtilisateur = "";
		do {
			System.out.print(question);
			positionUtilisateur = Console.lireString().toUpperCase();
		} while (!positionUtilisateur.matches("[A-C][1-4]"));
		
		int indiceLigne = positionUtilisateur.charAt(0) - 'A';
		int indiceCol = Integer.parseInt(positionUtilisateur.substring(1)) - 1;
		int[] position = {indiceLigne, indiceCol};

		return position;
	}
	
	/**
	 * Pose une question et lit la position donnée par l'utilisateur et la transforme en une position valide d'un tableau à deux dimensions. 
	 * Vérifie également que la position en question n'est pas déjà visible
	 * @param question String posant la question à l'utilisateur
	 * @param visibiliteNumeros Le tableau définissant si chaque position doit être visible ou non 
	 * @return Le tableau d'entier correspondant à la position dans la grille
	 */
	public static int[] lirePosition(String question, boolean[][] visibiliteNumeros) {
		int[] position = new int[2];
		do {
			position = lirePosition(question);
		} while (visibiliteNumeros[position[0]][position[1]]);
		return position;
	}
	
	/**
	 * Effectue l'aquisition d'un choix par l'utilisateur
	 * @param question La question à poser
	 * @param options Les différentes options acceptée (tel que "ER" veut dire accepter 'E' et accepter 'R')
	 * @return L'option choisie
	 */
	public static char lireChoix(String question, String options) {
		char choix = '*';
		options = options.toUpperCase();
		do {
			System.out.print(question);
			choix = Character.toUpperCase(Console.lireChar());
		} while (options.indexOf(choix) == -1 && options.length() != 0);
		return choix;
	}
}
